const markdownIt = require("markdown-it");

module.exports = function (eleventyConfig) {

    eleventyConfig.addPassthroughCopy("_img/**/*.*");
    eleventyConfig.addPassthroughCopy("_css/**/*.*");
    eleventyConfig.addPassthroughCopy("favicon.ico", "/");
    eleventyConfig.addPassthroughCopy(".htaccess", "/");
    eleventyConfig.addPassthroughCopy("script.js");

    //markdown
    let options = {
        html: true, // Enable HTML tags in source
        breaks: true,  // Convert '\n' in paragraphs into <br>
        linkify: true // Autoconvert URL-like text to links
    };

    //set the library to process markdown files
    eleventyConfig.setLibrary("md", markdownIt(options));

};